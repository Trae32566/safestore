Name: safestore
Version: 3.0.3
Release: 1.0.0
Summary: Backup program with archival and rotation functionality
License: GPLv3+
Source: safestore-%{version}.tar.gz
BuildArch: noarch
BuildRequires: bzip2, checkpolicy, git, help2man, make, policycoreutils, python3 >= 3.2, selinux-policy-devel, systemd
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
Requires: rsync, python3 >= 3.2, systemd
Requires(post): policycoreutils
Requires(postun): policycoreutils
# Disable debuginfo since it's noarch
%global debug_package %{nil}

%description
Uses multiple methods to backup, rotate, or restore a server's backups. Current methods include:
rsync, MySQL-python (for MySQL), LVM (LVM snapshots), XFS [WIP] (XFS snapshots), and rsyncd

%pre
%define ssSrcDir %{_builddir}/safestore-%{version}

%prep
%autosetup -D -n %{_builddir}

%changelog
* Wed Sep 22 2021 Trae Santiago <trae32566@gmail.com> - 3.0.3-1.0.0
    - Fixed bad CLI argument testing
* Mon May 10 2021 Trae Santiago <trae32566@gmail.com> - 3.0.2-1.0.0
    - Updated man pages for correctness
    - Updated exit codes for correctness
    - Fixed imp deprecation
    - Removed logging before loading safestore module
    - Added import of sys and moved to using the full module name
* Mon May 10 2021 Trae Santiago <trae32566@gmail.com> - 3.0.1-1.0.0
    - Fixed bug causing invalid delta directories
    - Updated SELinux policy to allow unlinking
* Sat May 8 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-1.0.2
    - Fixed incorrect man page notes
* Sat May 8 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-1.0.0
    - Tightened up SELinux policy way more and fixed man pages
* Sat May 8 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.rc12
    - Tightened up SELinux policy
* Sat May 8 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.rc11
    - Fixed an incorrect index in ConfigError
    - Added missing exit codes for exceptions
* Fri May 7 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.rc10
    - Improved cli missing argument handling on Python 3
    - Improved error handling and return codes
    - Improved module finding code
    - Improved SELinux policy to include rotate functionality coverage
    - Removed broken unit tests and added plans for functional tests
    - Started work on deprecated imports from imp
    - Updated specfile to correctly check for SELinux
    - Updated specfile to restrict config and secret permissions
* Sun Mar 28 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.rc1
    - Updated README
* Sun Mar 28 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.b5
    - Broke out different pieces of Core() into their own classes and files
    - Migrated async garbage to threading
    - Moved to 120 character line length limit
    - Removed extraneous bits of SELinux policy
    - Moved to python3 default!
* Fri Mar 19 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.b4
    - Fixed a major bug preventing async backups
* Fri Mar 19 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.b3
    - SELinux policy finalized
    - Fixed exception text
    - Updated version to give more info
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a31
    - More selinux fixes
    - Set sebools
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a15
    - Forced restorecon to fix selinux user not being changed
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a14
    - More work on SELinux
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a13
    - Moved to using corecmd_exec_bin
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a12
    - Fixed SELinux issue with missing bin_t
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a11
    - Fixed SELinux issue with mmaping python
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a10
    - Fixed policy install
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a8
    - Fixed quotes
* Thu Mar 18 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a7
    - Fixed makefile location
* Wed Mar 17 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a5
    - Moved to makefile
* Wed Mar 17 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a4
    - Added checkmodule for safestore policy
* Wed Mar 17 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a3
    - Fixed fcontext location
* Tue Mar 16 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a2
    - Disable debuginfo
    - Added Safestore SELinux policy
* Sun Mar 14 2021 Trae Santiago <trae32566@gmail.com> - 3.0.0-0.0.a1
    - Added rsycd SELinux policy and related BuildRequires
    - Bumped Python requirement to 3.4+ (2.x code remains for now but requires manual python2 install)
    - Continued Python 3 migration
    - Improved headers
* Thu Nov 12 2020 Trae Santiago <trae32566@gmail.com> - 2.3.7-3.0.0
    - Updated safestore to load module later on with logging
* Thu Nov 12 2020 Trae Santiago <trae32566@gmail.com> - 2.3.6-3.0.0
    - Fixed permissions on binary
* Thu Nov 12 2020 Trae Santiago <trae32566@gmail.com> - 2.3.6-1.0.0
    - Updated man pages
    - Added better descriptions in examples
    - Added python2 and python3 as BuildRequires
    - Removed unneeded specfile definition
    - Removed static version from safestore and integrated versioning via specfile
* Wed Nov 11 2020 Trae Santiago <trae32566@gmail.com> - 2.3.5-4.0.0
    - Final specfile fixes
* Wed Nov 11 2020 Trae Santiago <trae32566@gmail.com>
    - Added tito build instructions and fixed systemd services so they background


%install
subCmdStrip='/^(\.(T|S)H|\.\\\"|usage: \\-).*|.*manual page.*/ d'

# Create dirs
mkdir -p %{buildroot}{%{_bindir},%{_datadir}/selinux/packages/,%{_libdir}/%{name},%{_sysconfdir}/%{name},%{_unitdir}/%{name},%{_mandir}/{'man1','man5','man8'}}

# Insert version and copy files
sed 's/DEVEL/%{version}-%{release}/' "%{ssSrcDir}/%{name}.py" > "%{buildroot}%{_bindir}/%{name}.py"
chmod +x "%{buildroot}%{_bindir}/%{name}.py"
cp -ar "%{ssSrcDir}/%{name}" "%{buildroot}%{_libdir}/%{name}/"
cp -ar "%{ssSrcDir}/%{name}.json.example" "%{buildroot}%{_sysconfdir}/%{name}/"
cp -ar "%{ssSrcDir}/services/systemd/"* "%{buildroot}%{_unitdir}"

# Autoconfigure date + version in manpages
sed "s/\"<date>\" \"<version>\"/\"$(date +%Y-%m-%d)\" \"%{version}-%{release}\"/" "%{ssSrcDir}/man/%{name}.json.5" | gzip -c > "%{buildroot}%{_mandir}/man5/%{name}.json.5.gz"
sed "s/\"<date>\" \"<version>\"/\"$(date +%Y-%m-%d)\" \"%{version}-%{release}\"/" "%{ssSrcDir}/man/%{name}.8" | gzip -c > "%{buildroot}%{_mandir}/man8/%{name}.8.gz"

# Build manpages from help
gzip -c > "%{buildroot}%{_mandir}/man1/%{name}.1.gz" << SAFESTOREMAN
$(help2man -N --no-discard-stderr -i "%{ssSrcDir}/man/%{name}.1.h2m" "%{ssSrcDir}/%{name}.py")
.SS \fIBackup\fR
$(help2man -N --version-string='0.1' "%{ssSrcDir}/%{name}.py --verbosity CRITICAL backup" | sed -r "${subCmdStrip}")
.SS \fIRestore\fR
$(help2man -N --version-string='0.1' "%{ssSrcDir}/%{name}.py --verbosity CRITICAL restore" | sed -r "${subCmdStrip}")
.SS \fIRotate\fR
$(help2man -N --version-string='0.1' "%{ssSrcDir}/%{name}.py --verbosity CRITICAL rotate" | sed -r "${subCmdStrip}")
.RE
$(cat "%{ssSrcDir}/man/%{name}.1")
SAFESTOREMAN

# Create policy module for safestore
make -f /usr/share/selinux/devel/Makefile -C "%{ssSrcDir}/policies/"
cp "%{ssSrcDir}/policies/%{name}.pp" "%{buildroot}%{_datadir}/selinux/packages/"
# Create policy module for safestore-rsyncd
checkmodule -M -m -o "%{_builddir}/%{name}-rsyncd.mod" "%{ssSrcDir}/policies/%{name}-rsyncd.te"
semodule_package -o "%{buildroot}%{_datadir}/selinux/packages/%{name}-rsyncd.pp" -m "%{_builddir}/%{name}-rsyncd.mod"
# Package and move
bzip2 %{buildroot}%{_datadir}/selinux/packages/%{name}{,-rsyncd}.pp


%files
%{_bindir}/%{name}.py
%{_libdir}/%{name}/
%{_mandir}/man1/%{name}.1.gz
%{_mandir}/man8/%{name}.8.gz
%{_mandir}/man5/%{name}.json.5.gz
%attr(0700, -, -) %{_sysconfdir}/%{name}/
%attr(0600, -, -) %{_sysconfdir}/%{name}/*
%{_sysconfdir}/%{name}/%{name}.json.example
%{_unitdir}/%{name}-*
%{_datadir}/selinux/packages/%{name}.pp.bz2
%{_datadir}/selinux/packages/%{name}-rsyncd.pp.bz2


%clean
rm -rf %{buildroot}


%postun
if [ "${1}" = 0 ]; then
    # Unload SELinux policy
    semodule -nr safestore safestore-rsyncd
    # Reload systemd
    systemctl daemon-reload
    # Remove symlink
    rm %{_bindir}/%{name}
fi


%post
# Create a symlink to the .py if it doesn't exist
if ! [ -e %{_bindir}/%{name} ]; then
    ln -sf %{_bindir}/%{name}.py %{_bindir}/%{name}
fi

# Check if SELinux is enabled and load policy
selinuxenabled && if [ $? -eq 0 ]; then
    # Load SELinux policy
    semodule -ni %{_datadir}/selinux/packages/%{name}{,-rsyncd}.pp.bz2
    load_policy
    # Restore contexts
    restorecon -FR %{_bindir}/%{name}.py %{_sysconfdir}/%{name} %{_libdir}/%{name}
    # Set sebools for rsync backups
    for sebool in rsync_client rsync_export_all_ro rsync_full_access; do
        if [ $(getsebool "${sebool}" | awk '{ print $3 }') == 'off' ]; then boolStr="${boolStr} ${sebool}=1 "; fi
    done
    if [ ! -z "${boolStr}" ]; then setsebool -P ${boolStr}; fi
fi

# Force config and secret to not be world-readable
chmod o-rwx /etc/safestore/*
setfacl -m 'd:o:---' %{_sysconfdir}/%{name}

# Reload systemd
systemctl daemon-reload