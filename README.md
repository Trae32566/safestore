# Safestore #
Lightweight multi-function backup solution

## About Safestore ##
This project was created to provide an **easy, free, high performance** way to configure reliable backups for almost any infrastructure.

+ Compatible Operating Systems:
    * Red Hat Enterprise Linux / CentOS 7
    * Red Hat Enterprise Linux 8
    * CentOS Stream
    * Fedora 30+
+ Potential Operating Systems:
    * Red Hat Enterprise Linux 6 (no packaging, services, or cron)
    * Debian (no packaging)
    * Anything systemd based with rsync
    
## Features ##
### Current ###
+ Python Compatibility:
    * Python 2.6+
    * Python 3.2+
+ Backups:
    * Asynchronous and synchronous backups
    * Parallel and serial backup configurations
    * Bandwidth limiting for any remote URIs
    * File exclusions
    * Filesystem boundary detection
    * Snapshot based backups for LVM
    * Database locking support for snapshots
    * Password protected remote rsync
    * Remote functionality by specifying the source as a remote source
+ Restore:
    * Asynchronous and synchronous restoration
    * Bandwidth limiting for any remote URIs
    * File exclusions
    * Filesystem boundary detection
    * Password protected remote rsync
+ Rotation:
    * Asynchronous and synchronous rotations
    * Parallel and serial rotation configurations

### Planned ###
* Add functional testing
* [WIP] Optimizations for job scheduling
* mlocate based optimizations 
* Plugin for unmounted filesystems (MountFS)
* Record keeping of deleted files (so that deletions can be played back when restoring)
+ Allow limitations and provide sane defaults, sometimes things can go sideways with increasing load:
      * Load average
      * Disk IO
      * CPU usage
      * Memory usage
      * Network activity
* Better logging when files fail to be copied instead of "Errors were encountered while copying files!" 
* Configuration templates for scalability with large numbers of devices
* More granular control over the number of jobs that can be run at once instead of all or 1
* [?] Add setup functionality for clients and servers
* Integrity verification of backups -- this could be done by writing the checksum from rsyncd to a file (maybe sign the file too?) 
* Bare metal backup / restore
* LVM-based VM backup / restore
* Better rotation schemes than just count (ex: Tower of Hanoi)
* Smarter rotation than just scanning directories and trying to see if it follows our backup scheme (maybe `.safestore-metadata`?)

## Building RPMs ##
Safestore utilizes [Tito](https://github.com/rpm-software-management/tito) for versioning and building. RPMs can be built by running:
```
tito build --rpm
```

## Notes / Caveats ##
See the man page for safestore(1) and safestore(5) for more information.

## Bugs / Errata ##
See the man page for safestore(1) and safestore(5) for more information.

## Configuration File ##
See the man page for safestore.json(5) for more information.

## Setup and Usage ##
See the man page for safestore(1) and safestore(5) for more information.
