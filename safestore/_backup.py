# safestore/_backup.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Safestore backup handler and worker

Backs up data asyncronously using rsync. Designed to be as modular as possible, so that alternate backup methods can be
added in the future. 
'''
from os import getcwd, listdir
from os.path import exists, isdir, ismount, join, pardir, sep
from pprint import pformat
from socket import gethostname
from subprocess import CalledProcessError, check_output, STDOUT
from sys import exc_info
from threading import Event, Thread
from time import gmtime, strftime

# Python 2 module name fallback
try:
    from queue import Queue, Empty
except ImportError:
    from Queue import Queue, Empty

# Local imports (yes, globs!)
from .exceptions import *

class _Backup():
    '''_Backup threaded handler for Safestore

    This class uses threading to perform asynchronous backups via rsync.
    '''
    def backup(self, directArgs=None):
        '''Backup handler, since _backup_worker() needs to be managed.

        Arguments:
        directArgs - A list of direct arguments to pass to every rsync instance. This overrides the
        default arguments that we give it, so make sure to include arguments needed, EXCEPT THOSE 
        REQUESTED BY CONFIGURATION PARAMETERS (ie: bwCap). As well, the first (parent) queue item 
        created for asynchronous backups will have an additional flag, so that it ignores all 
        subdirectories. All subsequent rsync calls will not have this.

        A few caveats here:
        o There is no guarantee that this will not cause system instability due to high load
        o Eventually an _optimize() method will be added to calculate the ideal load distribution,
            but for now we're just using a basic top level directory crawling method.
        '''
        self.deadTree.info('Starting backup activity')

        # Make a queue for backup tasks
        self.bkpQueue = Queue()

        # Create an event to let the workers know when to die
        self.bkpComplete = Event()

        # Set a consistent start time
        self.startTime = strftime('%Y-%m-%d_%H-%M', gmtime())

        for source, bkpConfig in self.config['backup'].items():
            # Make sure the event is not complete yet
            if self.bkpComplete.is_set(): self.bkpComplete.clear()

            # Plugin stuffs
            # Exception handling here is CRITICAL, we need to make sure these plugins fail in a 
            # predictable way to prevent Safestore from exploding!
            if source[0] == ':' and source[-1] == ':':
                try:
                    pluginMod = safestore_import(pluginName=source[1:-1])
                except ModuleNotFoundError as err:
                    raise ConfigError(self.config, err, exc_info(), self.deadTree)

                for method, objName in bkpConfig.items():
                    try:
                        # Pass everything after the plugin name
                        pluginMod.plugin_wrapper(
                            method, 
                            objName.keys()[-1], 
                            self.config['backup'][source], 
                            self.deadTree
                        )
                    except:
                        raise PluginError(exc_info(), source[1:-1], self.deadTree)
            else:
                # Set async defaults
                if not 'async' in bkpConfig: bkpConfig['async'] = {}
                if not 'maxThreads' in bkpConfig['async']: bkpConfig['async']['maxThreads'] = 1
                if not 'pollInterval' in bkpConfig['async']: bkpConfig['async']['pollInterval'] = 0.01
                
                # Warn if we don't have a trailing '/'
                if source[-1] != sep:
                    self.deadTree.warning('Trailing separator missing; this defeats the purpose of async!')

                for count in range(bkpConfig['async']['maxThreads']):
                    bkpThread = Thread(target=self._backup_worker, args=(bkpConfig['async']['pollInterval'],))
                    bkpThread.daemon = True
                    bkpThread.start()

                flags = ['-abixzAS', '--delete'] if directArgs is None else directArgs

                # Single threaded (any source which is not a file, ie: rsync URI, is not async!)
                if bkpConfig['async']['maxThreads'] == 1 or not isdir(source):
                    self.bkpQueue.put((source, None, flags))
                # Multi-threaded
                else:
                    # The parent portion here is mandatory
                    parentFlags = flags[:]
                    parentFlags.extend(['--filter=-,p */', '-d'])

                    # Crawl the top level (this is very basic and limits performance severely with few base dirs)
                    for item in listdir(source):
                        fullPath = join(source, item)

                        if isdir(fullPath) and not ismount(fullPath):
                            item += sep
                            self.bkpQueue.put((source, item, flags))
                    
                    # Make sure we run the parent dir backup LAST
                    self.bkpQueue.put((source, None, parentFlags))

            # *default* is serial. Don't get confused!
            if not 'safestore' in self.config or not 'serial' in self.config['safestore'] or self.config['safestore']['serial']:
                self.bkpQueue.join()
                
                if hasattr(self, 'explosion'): 
                    raise self.explosion
                else:
                    self.bkpComplete.set()
                    self.deadTree.info('Serial backup complete!')
            else:
                self.deadTree.info('Workers started for this backup!')

        if 'safestore' in self.config and 'serial' in self.config['safestore'] and not self.config['safestore']['serial']:
            self.bkpQueue.join()

            if hasattr(self, 'explosion'):
                raise self.explosion
            else:
                self.bkpComplete.set()
                self.deadTree.info('Backup complete!') 

    def _backup_worker(self, timeout):
        '''The "meat" of the backup process

        This is an internal method called by Core().backup() for each thread instance.
        Note: This uses --exclude, NOT --exclude-from=- because passing stdin is a pain.
        '''
        while not self.bkpComplete.is_set():
            try:
                source, subDir, directArgs = self.bkpQueue.get(True, timeout)
            except Empty:
                continue

            # Make sure we die gracefully to prevent hanging
            try:
                # Leave the path alone if we don't specify a subdir
                fullDir = source if subDir is None else join(source, subDir)

                destDir = (
                    self.config['backup'][source]['destination'] 
                    if subDir is None
                    else '{0}{1}{2}'.format(self.config['backup'][source]['destination'], sep, subDir[:-1])
                )

                # Create the prefix for deltas
                prefix = (
                    gethostname().split('.')[0] if not 'prefix' in self.config['backup'][source]
                    else self.config['backup'][source]['prefix']
                )

                # Set the directory for deltas
                if subDir is None:
                    deltaDir = '{0}_{1}'.format(prefix, self.startTime)
                # Modify delta directory to include the async subdir
                else:
                    deltaDir = '{0}{1}_{2}{3}{4}'.format(
                        (pardir + sep) * (subDir[:-1].count(sep) + 1),
                        prefix,
                        self.startTime,
                        sep,
                        subDir[:-1]
                    )

                protectDir = (
                    gethostname().split('.')[0]
                    if not 'prefix' in self.config['backup'][source]
                    else self.config['backup'][source]['prefix']
                )

                self.deadTree.info("Copying '{0}'".format(fullDir))
                rsyncArgs = ['rsync', '--filter=protect {0}*'.format(protectDir), fullDir, destDir]

                # We don't always want deltas
                if not 'deltas' in self.config['backup'][source] or self.config['backup'][source]['deltas']:
                    rsyncArgs.insert(-2, '--backup-dir={0}'.format(deltaDir))

                # Bandwidth cap
                if 'bwCap' in self.config['backup'][source]:
                    rsyncArgs.insert(-2, '--bwlimit={0}'.format(self.config['backup'][source]['bwCap']))

                # Password file
                if 'passFile' in self.config['backup'][source]:
                    rsyncArgs.insert(-2, '--password-file={0}'.format(self.config['backup'][source]['passFile']))

                # Exclusion list
                if 'excludes' in self.config['backup'][source]:
                    for exclude in self.config['backup'][source]['excludes']:
                        if subDir is None:
                            rsyncArgs.insert(-2, '--exclude={0}'.format(exclude))
                        elif exclude.find(subDir) != -1:
                            rsyncArgs.insert(-2, '--exclude={0}'.format(exclude.replace(subDir, '', 1)))

                # Add arguments
                [ rsyncArgs.insert(1, directArg) for directArg in directArgs ]

                self.deadTree.debug('subprocess.Popen() command list:\n{0}'.format(rsyncArgs))

                # We're using close_fds=True to work around subprocess.Popen() not being thread safe:
                # https://stackoverflow.com/questions/21194380/is-subprocess-popen-not-thread-safe#comment31948801_21194380
                rsyncInstance = check_output(rsyncArgs, close_fds=True, stderr=STDOUT)
                self.deadTree.info("Finished copying '{0}'".format(fullDir))
            except CalledProcessError as err:
                if err.returncode in self.retWarnActions:
                    self.deadTree.warning(self.retWarnActions[err.returncode])
                    self.deadTree.debug('Rsync output was:\n'.format(err.output))
                else:
                    self.explosion = RsyncError(
                        err,
                        self.deadTree,
                        killEvent=self.bkpComplete, 
                        childQueue=self.bkpQueue
                    )
            except OSError as err:
                self.deadTree.debug('Error code: {0}'.format(err.errno))
                self.deadTree.debug('Error description: {0}'.format(err.strerror))
                if hasattr(err, 'filename'): self.deadTree.debug('File: {0}'.format(err.filename))
                self.explosion = UnknownChildError(self.bkpComplete, self.bkpQueue, exc_info(), self.deadTree)
            except:
                self.explosion = UnknownChildError(self.bkpComplete, self.bkpQueue, exc_info(), self.deadTree)

            # ALWAYS set the task done
            self.bkpQueue.task_done()