# safestore/exceptions.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Exception handling classes for Safestore

Provides exception handling for Safestore, as well as basic exception handling for plugins.
'''
from traceback import print_tb
from pprint import pformat

########################################
#       Base Exception Handlers        #
########################################
class SafestoreError(Exception):
    '''Generic Safestore error base class

    Provides a base class for errors which occur in the main (parent) thread.
    '''
    pass

class ChildError(SafestoreError):
    '''Child thread error base class

    Provides a class for errors which occur in spawned (child) threads.
    '''
    def kill_workers(self, childQueue, killEvent):
        '''Kill all child workers

        Called to destroy any remaining tasks and set the event to kill workers.
        '''
        # Go ahead and kill off the children
        self.deadTree.info('Triggering the kill event')
        killEvent.set()

        # Empty the Queue that gets created in Core()
        self.deadTree.info('Emptying the queue')
        while not childQueue.empty():
            childQueue.get()
            childQueue.task_done()
        self.deadTree.info("Okay, I think we're dead!")

class PluginError(SafestoreError):
    '''Safestore plugin exception base class

    This was created foremost as a base class for plugin creators (maybe one day) to use, however,
    it also allows us to create a generic plugin exception handler. This will only take action 
    itself if the plugin developer does not subclass this.
    '''
    def __init__(self, originErr, plugin, treeInstance):
        self.deadTree = treeInstance.getChild('PluginError')

        if issubclass(originErr, PluginError):
            raise originErr
        else:
            self.deadTree.critical('An unknown error has occurred in a plugin!')
            self.deadTree.debug('Original traceback:')
            # In this case we use originErr as a traceback handler instead of an actual error
            print_tb(originErr[2])
            exit(50)

#######################################
#         Safestore Exceptions        #
#######################################
class ConfigError(SafestoreError):
    '''Exception handler for configuration issues

    Called when any configuration-related error occurs.
    '''
    def __init__(self, config, originErr, originTb, treeInstance):
        self.deadTree = treeInstance.getChild('ConfigError')

        configExceptions = { 
            'FileNotFoundError'     : (41, 'Configuration file {0} missing!'.format(config)),
            'JSONDecodeError'       : (42, 'Configuration file {0} JSON syntax invalid!'.format(config)),
            'KeyError'              : (43, "Unable to find configuration section: '{0}'!".format(originErr.args[0])),
            'PermissionError'       : (44, 'Configuration file {0} inaccessible due to permissions!'.format(config)),
            'ModuleNotFoundError'   : (45, originErr.args[0].replace('module', 'plugin')) # Use the original error
        }

        self.deadTree.debug('Configuration was:\n{0}'.format(pformat(config)))
        self.deadTree.debug('Original traceback:')
        print_tb(originTb[2])

        try:
            self.deadTree.critical(configExceptions[originErr.__class__.__name__][1])
            exit(configExceptions[originErr.__class__.__name__][0])
        except KeyError:
            self.deadTree.critical('An unknown configuration error occurred: {0}'.format(originErr.__class__.__name__))
            exit(40)

class RestoreDateError(SafestoreError):
    '''Exception handler for invalid dates

    Called when checking to see if the date given exists in the list of potential restore dates.
    '''
    def __init__(self, deltaDates, restoreDate, treeInstance):
        self.deadTree = treeInstance.getChild('RestoreDateError')
        self.deadTree.critical('Unable to restore; the restore date provided is unavailable!')
        self.deadTree.debug('Restore date provided: {0}'.format(restoreDate))
        self.deadTree.debug(
            'Restore dates available:\n{0}'.format(
                '\n'.join([deltaDate.strftime('%Y-%m-%d_%H-%M') for deltaDate in deltaDates])
            )
        )
        exit(60)

class RsyncError(ChildError):
    '''Exception handler for rsync issues

    Called when a fatal exception is given by rsync. This kills any existing children, and provides
    error messages along with a return code.
    '''
    def __init__(self, originErr, treeInstance, killEvent=None, childQueue=None):
        self.deadTree = treeInstance.getChild('RsyncError')

        # Missing codes are due to the man page being unclear as to whether they cause an
        # immediate exit, or just a warning.
        # Missing: 3, 4, 6, 11, 12, 13, 14, 21
        exitCodes = {
            1: 'Rsync syntax or usage error; possibly bad authentication.',
            2: 'Protocol incompatibility!',
            5: 'Error with client <-> server communication; check rsyncd logs for more information.',
            10: 'Error with destination location; verify destination is accessible.',
            11: 'Error with file IO; verify destination is writeable.',
            20: 'SIGUSR1 or SIGINT was received!',
            22: 'Error allocating memory!',
            # This only happens if we explicitly exclude it from Core.retWarnActions() to cause an error
            23: 'An incorrect source has been specified!',
            30: 'Timed out while sending / receiving data!',
            35: 'Timed out waiting for daemon connection!'
        }

        # If we're passed a queue, we need to kill workers
        if childQueue is not None: self.kill_workers(childQueue, killEvent)

        # See if we know what the issue is
        if originErr.returncode in exitCodes:
            self.deadTree.critical(exitCodes[originErr.returncode])
        else:
            self.deadTree.critical(
                '''An ambiguous or unknown error has occurred!
rsync exit code: {0}'''.format(originErr.returncode)
            )
        self.deadTree.debug('Rsync output was:\n'.format(originErr.output))
        #exit(originErr.returncode) 

class UnknownChildError(ChildError):
    '''Exception handler for unknown errors occurring inside a child thread

    Called when any exception not explicitly handled is raised, so that threads do not hang.
    '''
    def __init__(self, childQueue, killEvent, originErr, treeInstance):
        self.deadTree = treeInstance.getChild('UnknownChildError')

        self.kill_workers(childQueue, killEvent)
        self.deadTree.critical('An unknown error has occurred in a child thread!')
        self.deadTree.debug('Original traceback: {0}'.format(originErr[0]))
        print_tb(originErr[2])
        #exit(61)