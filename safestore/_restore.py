# safestore/_restore.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Safestore restoration module

This module gives information about restore points and restores backups asyncronously.
'''
from collections import OrderedDict
from datetime import datetime
from os.path import join, sep
from pprint import pformat
from subprocess import CalledProcessError, check_output, STDOUT
from time import strftime

# Local imports (yes, globs!)
from .exceptions import *

class _Restore():
    '''Restore for Safestore

    This class uses Core.backup() to perform asynchronous restorations using rsync.
    '''
    #########################
    #    Restore stuffs     # 
    #########################    
    def restore(self):
        '''Restore method for restoring Safestore backups

        This makes use of Core.backup() to restore backups, including deltas to a specific 
        time. This does not verify the integrity of the backup in any way!
        '''
        self.deadTree.info('Starting restore activity')
        # Python 2 returns a list for .keys(), while Python 3 returns dict_keys, so we force it
        # https://stackoverflow.com/questions/17322668/typeerror-dict-keys-object-does-not-support-indexing
        rstrSrc = list(self.config['restore'].keys())[0]
        deltaDates = []

        # We use rsync to get the directories because we need to do it for remote URIs
        rsyncArgs = [
            'rsync',
            '-nr',
            '--out-format=%n',
            '--filter=-! *[[:digit:]]*-[[:digit:]]*-[[:digit:]]*_[[:digit:]]*-[[:digit:]]*/',
            '{}'.format(rstrSrc if rstrSrc[-1] == sep else rstrSrc + sep) # Make sure there's a separator
        ]
        
        if 'passFile' in self.config['restore'][rstrSrc]:
            rsyncArgs.insert(-2, '--password-file={0}'.format(self.config['restore'][rstrSrc]['passFile']))
        
        try:
            rsyncOutput = check_output(rsyncArgs, close_fds=True, stderr=STDOUT)
        except CalledProcessError as err:
            # 23 is the error returned if the directory is incorrect, which in this case is a failure
            if err.returncode in self.retWarnActions and not 23:
                self.deadTree.warning(self.sys.argv[err.returncode])
                self.deadTree.debug('rsync output was:\n'.format(err.output))
            else:
                raise RsyncError(err, self.deadTree)

        # The first result is always current working directory (.) and the last is always newline
        # We have to make sure that rsyncOutput is always decoded (Python 3 returns bytes)
        deltaDirs = rsyncOutput.decode('utf-8').split('\n')[1:-1]
        self.deadTree.debug('rsync arguments:\n{0}'.format(pformat(rsyncArgs)))

        for deltaDir in deltaDirs:
            try:
                deltaDates.append(datetime.strptime(deltaDir[-16:-1], '%Y-%m-%d_%H-%M'))
            except ValueError:
                self.deadTree.info('Ignoring invalid delta directory: {0}'.format(deltaDir))
                deltaDirs.remove(deltaDir)
        
        if 'list' in self.config['restore'][rstrSrc]:
            print('Available restore dates:')
            for deltaDate in deltaDates: print(deltaDate.strftime('%Y-%m-%d_%H-%M')) 
        else:
            self.deadTree.info('Starting restore process')
            # These config elements are almost identical except for date
            self.config['backup'] = OrderedDict()
            self.config['backup'][rstrSrc] = self.config['restore'][rstrSrc]
            
            if 'date' in self.config['restore'][rstrSrc]:
                try: 
                    tgtIndex = deltaDates.index(self.config['restore'][rstrSrc]['date']) + 1
                except ValueError:
                    raise RestoreDateError(
                        deltaDates,
                        self.config['restore'][rstrSrc]['date'].strftime('%Y-%m-%d_%H-%M'),
                        self.deadTree
                    )

                # Iterate around all the deltas prior to the one they selected, because the folder 
                # with the date they selected contains delta changes from the previous backup. 
                for rstrDir in deltaDirs[-1:tgtIndex - 1:-1]:
                    self.deadTree.debug('Adding {0}'.format(rstrDir))
                    self.config['backup'][join(rstrSrc, rstrDir)] = self.config['restore'][rstrSrc]
                    # We don't want deltas when restoring
                    self.config['backup'][join(rstrSrc, rstrDir)]['deltas'] = False

                self.deadTree.debug('Configuration is:\n{0}'.format(pformat(self.config)))

            self.backup(directArgs=[
                '-abxzAS', 
                '--filter=- *[[:digit:]]*-[[:digit:]]*-[[:digit:]]*_[[:digit:]]*-[[:digit:]]*/'
            ])