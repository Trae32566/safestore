# safestore/__init__.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Safestore module

This module provides backup, restore, and rotation functionality.
'''
from logging import basicConfig, captureWarnings, getLogger, INFO

# Local imports (yes, globs!)
from ._backup import _Backup
from ._restore import _Restore
from ._rotate import _Rotate
from .exceptions import *

class Core(_Backup, _Restore, _Rotate):
    '''Core threaded handler for Safestore

    This parent class provides logging and unification for other classes.
    '''
    def __init__(self, config, treeInstance=None, verbosity=INFO):
        if treeInstance:
            self.deadTree = treeInstance.getChild('Core')
            if 'safestore' in config and 'verbosity' in config['safestore']: 
                self.deadTree.setLevel(config['safestore']['verbosity'])
        else:
            # Get a dead tree setup ASAP
            basicConfig(
                format='%(asctime)s | %(levelname)s - %(name)s.%(funcName)s @ %(lineno)d: %(message)s',
                level=verbosity
            )
            captureWarnings(True)
            self.deadTree = getLogger('Core')


        # Missing codes are due to the man page being unclear as to whether they cause an immediate 
        # exit, or just a warning.
        self.retWarnActions = {
            23: 'Errors were encountered while copying files!',
            24: 'File(s) vanished when attempting to copy them!',
            25: 'Deletions were stopped due to maxDeletions setting!'
        }

        # Set the configuration
        self.config = config
        self.deadTree.info('Core instance created!')
