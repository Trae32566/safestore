# safestore/_rotate.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Safestore rotation module

This module asyncronously rotates backups.
'''
from glob import glob
from os import listdir
from os.path import isdir, join
from pprint import pformat
from shutil import rmtree
from sys import exc_info
from threading import Event, Thread

# Python 2 module name fallback
try:
    from queue import Queue, Empty
except ImportError:
    from Queue import Queue, Empty

# Local imports (yes, globs!)
from .exceptions import *

class _Rotate():
    '''Core threaded (but not truly) handler for Safestore

    This class uses threading to perform asynchronous backups using rsync.
    '''
    def rotate(self):
        '''Rotate wrapper, since _rotate_worker() needs to be queued.

        There is no guarantee that this will not cause system instability due to high load.
        '''
        self.deadTree.info('Starting rotation activity')

        # Make a queue
        self.rotQueue = Queue()

        # Create an event to let the workers know when to die
        self.rotComplete = Event()
        try:
            for source, rotConfig in self.config['rotate'].items():
                # Make sure the event is not complete yet
                if self.rotComplete.is_set(): self.rotComplete.clear()
    
                # Set async defaults
                if not 'async' in rotConfig: rotConfig['async'] = {}
                if not 'maxThreads' in rotConfig['async']: rotConfig['async']['maxThreads'] = 1
                if not 'pollInterval' in rotConfig['async']: rotConfig['async']['pollInterval'] = 0.01
    
                for count in range(rotConfig['async']['maxThreads']):
                    rotThread = Thread(target=self._rotate_worker, args=(rotConfig['async']['pollInterval'],))
                    rotThread.daemon = True
                    rotThread.start()
    
                # Single threaded
                if rotConfig['async']['maxThreads'] == 1:
                    self.rotQueue.put((source, None))
                # Multi-threaded
                else:
                    # Crawl the top level
                    # Needs to be replaced with Core.schedule()
                    for item in listdir(source):
                        if isdir(join(source, item)): self.rotQueue.put((source, item))
    
                if not 'serial' in self.config['safestore'] or self.config['safestore']['serial']:
                    self.rotQueue.join()
                    if hasattr(self, 'explosion'):
                        raise self.explosion
                    else:
                        self.rotComplete.set()
                        self.deadTree.info('Serial rotation complete!')
                else:
                    self.deadTree.info('Workers started for this rotation!')
        except KeyError as err:
            raise ConfigError(self.config, err, exc_info(), self.deadTree)

        if 'serial' in self.config['safestore'] and not self.config['safestore']['serial']:
            self.rotQueue.join()
            if hasattr(self, 'explosion'):
                raise self.explosion
            else:
                self.rotComplete.set()
                self.deadTree.info('Rotation complete!')

    def _rotate_worker(self, timeout):
        '''The "meat" of the rotation process

        This is an internal method called by Core().rotate() for each thread instance.
        '''
        while not self.rotComplete.is_set():
            try:
                source, subDir = self.rotQueue.get(True, timeout)
            except Empty:
                continue

            # We catch all exceptions, then pass them to the parent to be raised, thus preventing hung threads
            try:
                fullDir = join(source, subDir)

                for clientDir in listdir(fullDir):
                    self.deadTree.info("Rotating '{0}'".format(join(fullDir, clientDir)))

                    deltaDirs = glob('{0}/{1}*/'.format(join(fullDir, clientDir), subDir))
                    if deltaDirs:
                        deltaDirs.sort()
                        excess = len(deltaDirs) - int(self.config['rotate'][source]['count'])

                        while excess > 0:
                            self.deadTree.info('{0} excess backup(s), removing: {1}'.format(excess, deltaDirs[0]))
                            rmtree(deltaDirs[0])

                            # Decrement
                            excess -= 1
                            deltaDirs.pop(0)
            except: 
                self.explosion = UnknownChildError(self.rotQueue, self.rotComplete, exc_info(), self.deadTree)
            
            # ALWAYS set the task done
            self.rotQueue.task_done()