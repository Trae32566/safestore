# safestore/snapshot/xfs.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Provides XFS dump functionality for safestore.snapshot

    Returns an object representative of an open XFS dump, with basic functionality.
'''

class XFSSnap():
    '''Handles all prep work (config backup) and creates the XFS snapshot

    Something... 
    '''
    def __init__(self, objName, xfsSnapConfig, treeInstance, verbosity='DEBUG'):
        self.deadTree = treeInstance.getChild('XFSSnapshot')
        self.deadTree.setLevel(verbosity)

        self.objName = objName
        self.xfsSnapConfig = xfsSnapConfig
        self.deadTree.info('XFSSnapshot instance created')

        # Do prep work here

    def create(self):
        pass

    def destroy(self):
        pass