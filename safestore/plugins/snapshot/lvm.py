# safestore/snapshot/lvm.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
from json import loads
from os import mkdir, rmdir
from pprint import pformat
from subprocess import CalledProcessError, check_output, STDOUT
'''Provides LVM snapshot functionality for safestore.snapshot

    Returns an object representative of an open logical volume, with basic functionality.
'''

class LVMSnap():
    '''Handles all prep work (config backup) and creates the LVM snapshot.

    Creates a backup of the LVM statistics for the volume group and logical volume(s), then creates
    a snapshot when create() is called, and destroys it when destroy() is called. 
    '''
    def __init__(self, objName, lvmSnapConfig, treeInstance, verbosity='DEBUG'):
        self.deadTree = treeInstance.getChild('LVMSnapshot')
        self.deadTree.setLevel(verbosity)
        
        self.objName = objName
        self.lvmSnapConfig = lvmSnapConfig
        self.lvName = objName[objName.index('/') + 1:]
        self.deadTree.info('LVMSnapshot instance created')

        # Make sure we have defaults
        if not 'size' in self.lvmSnapConfig: self.lvmSnapConfig['size'] = 20 

        # Make sure we sanity check and do a backup of the current configuration
        self.preConfig = self._get_report()
        self._safety_check(self.preConfig)

    def create(self):
        self.deadTree.info('Creating an LVM snapshot of {0}'.format(self.objName))
        try:
            lvCreate = check_output([
                'lvcreate',
                '--extents', '{0}%ORIGIN'.format(self.lvmSnapConfig['size']),
                '--name', '{0}_ss'.format(self.objName),
                '--snapshot', self.objName
            ], stderr=STDOUT)
        except CalledProcessError as err:
            self.deadTree.critical('Unable to create LVM snapshot! Error was:\n{0}'.format(err.output))
            exit(err.returncode)

        mountCmd = [
            'mount', 
            '--options', 'nouuid,ro',
            '/dev/{0}_ss'.format(self.objName),
            '/tmp/{0}'.format(self.lvName)
        ]
        if 'fs' in self.lvmSnapConfig:
            mountCmd.insert(-2, '-t')
            mountCmd.insert(-2, self.lvmSnapConfig['fs'])

        try:
            # Should create the directory '/tmp/LV' and mount the snapshot
            mkdir('/tmp/{0}'.format(self.lvName))
            mount = check_output(mountCmd, stderr=STDOUT)

            return '/tmp/{0}/'.format(self.lvName)
        except CalledProcessError as err:
            self.deadTree.critical('Unable to create LVM snapshot! Error was:\n{0}'.format(err.output))               
            exit(err.returncode)

    def destroy(self):
        self.deadTree.info('Removing LVM snapshot of {0}'.format(self.objName))

        try:
            unmount = check_output(['umount', '/tmp/{0}'.format(self.lvName)], stderr=STDOUT)
            rmdir('/tmp/{0}'.format(self.lvName))
        except CalledProcessError as err:
            self.deadTree.critical('Unable to unmount LVM snapshot! Error was:\n{0}'.format(err.output))
            exit(err.returncode)

        try:
            lvRemove = check_output(['lvremove', '{0}_ss'.format(self.objName), '--yes'], stderr=STDOUT)
        except CalledProcessError as err:
            self.deadTree.critical('Unable to remove LVM snapshot! Error was:\n{0}'.format(err.output))
            exit(err.returncode)

    def _get_report(self):
        '''Gets and returns a JSON report of the full LVM configuration'''
        try:
            self.deadTree.info('Gathering configuration information')
            return loads(check_output([
                'lvm', 'fullreport', 
                '--reportformat', 'json',
                '--units', 'B'
            ], stderr=STDOUT))
        except CalledProcessError as err:
            self.deadTree.critical("'lvm fullreport' command failed!")
            self.deadTree.debug('Exit code: {0}\noutput:\n{1}'.format(err.returncode, err.output))
            exit(err.returncode)

    def _safety_check(self, lvmReport):
        '''Runs safety checks

        Checks to make sure configuration values are sane, and returns False if successful.
        '''

        # Find where in the report it is
        self.deadTree.info('Running safety checks')
        for vgInfo in lvmReport['report']:
            for lvInfo in vgInfo['lv']:
                if self.objName == lvInfo['lv_full_name'].lower(): 
                    vgIndex = lvmReport['report'].index(vgInfo)
                    lvIndex = vgInfo['lv'].index(lvInfo)

        if 'lvIndex' in locals() and 'vgIndex' in locals():
            lvSize = float(lvmReport['report'][vgIndex]['lv'][lvIndex]['lv_size'][:-1])
            snapSize = lvSize * (float(self.lvmSnapConfig['size']) / 100.0)
            vgFree = float(lvmReport['report'][vgIndex]['vg'][0]['vg_free'][:-1])

            self.deadTree.info('snapSize: {0}B'.format(snapSize))
            self.deadTree.info('lvSize: {0}B'.format(lvSize))
            
            if not snapSize <= vgFree:
                self.deadTree.critical('Snapshot size exceeds free space on volume group!')
                self.deadTree.debug('Snapshot size: {0}B ({1}%)\nFree space: {2}B'.format(
                    snapSize, 
                    self.lvmSnapConfig['size'],
                    vgFree
                ))
                return True

            self.deadTree.info('Safety checks completed successfully!')
            return False
        else:
            self.deadTree.critical('Logical volume {0} not found!'.format(self.objName))
            self.deadTree.debug('LVM report:\n{0}'.format(pformat(lvmReport)))
            return True
