# safestore/snapshot/db.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Provides database locking and unlocking functionality for safestore.snapshot'''

import MySQLdb
#import pymssql

class DBFreeze():
    '''Handles all prep work for the freeze'''
    def __init__(self, dbConfig, treeInstance, verbosity='DEBUG'):
        self.deadTree = treeInstance.getChild('DBFreeze')
        self.deadTree.setLevel(verbosity)

        self.dbConfig = dbConfig
        self.deadTree.info('DBFreeze instance created')

        # Set all the connection paramaters as necessary
        if dbConfig['type'].lower() == 'mysql':
            connArgs = { 'connect_timeout' : 15 if not 'timeout' in dbConfig else dbConfig['timeout'] }

            if 'host' in dbConfig: connArgs['host'] = dbConfig['host']
            if 'user' in dbConfig: connArgs['user'] = dbConfig['user']
            if 'password' in dbConfig: connArgs['passwd'] = dbConfig['password']
            if 'port' in dbConfig: connArgs['port'] = dbConfig['port']
            if 'socket' in dbConfig: connArgs['unix_socket'] = dbConfig['socket']

            self.freezeDB = MySQLdb.connect(**connArgs)
            self.dbCursor = self.freezeDB.cursor()
        else:
            self.deadTree.critical("Database type '{0}' unknown!".format(dbConfig['type'].lower()))

    def lock(self):
        '''Locks and flushes all databases in the MySQL instance.'''
        self.deadTree.info('Flushing tables with read lock')
        self.dbCursor.execute('FLUSH LOCAL TABLES WITH READ LOCK')
        # Make sure we set something so the caller can check if the DB is still locked
        self.locked = True

    def unlock(self):
        self.deadTree.info('Unlocking tables')
        self.dbCursor.execute('UNLOCK TABLES')
        self.dbCursor.close()
        # Make sure we set as unlocked
        self.locked = False