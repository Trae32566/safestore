# safestore/mountfs/__init__.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see 
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Provides snapshot functionality for safestore.Core() as a plugin

Designed to be as modular as possible, so that future snapshot methods may be added without major
code restructuring.
'''
from logging import basicConfig, captureWarnings, getLogger
from pprint import pformat

# Import safestore so we can run the actual backup
import safestore

class MountFS():
    '''Snapshot class for safestore.Core()

    This class provides snapshot functionality for safestore.Core() using multiple OS functions. 
    While this class is open using a with statement, it provides a path to back up which contains 
    the snapshot data.
    '''
    def __init__(self, method, objName, snapConfig, treeInstance, verbosity='DEBUG'):
        self.deadTree = treeInstance.getChild('Snapshot')
        self.deadTree.setLevel(verbosity)
        self.method = method
        self.objName = objName
        self.snapConfig = snapConfig

        self.deadTree.info('MountFS instance created')

    def __enter__(self):
        '''Snapshot wrapper, since we would prefer determining snapshot method here.

        This determines the snapshot method and calls the appropriate worker with arguments. Returns
        a name which is the file (or file descriptor) of the object to be backed up.
        '''
        snapMethods = {
            'lvm'   : lvm.LVMSnap,
            'xfs'   : xfs.XFSSnap
        }

        if 'dbInstance' in self.snapConfig[self.method][self.objName]:
            try:
                self.freezer = DBFreeze(
                    self.snapConfig['db'][self.snapConfig[self.method][self.objName]['dbInstance']],
                    self.deadTree
                )
                self.freezer.lock()
            except KeyError as err:
                self.deadTree.critical("Invalid database reference for {0} ('{1}')!".format(
                    self.objName, 
                    self.snapConfig[self.method][self.objName]['dbInstance']
                ))
                self.deadTree.debug('Error was:\n{0}'.format(pformat(err)))
                exit(31)

        try:
            self.snapInstance = snapMethods[self.method.lower()](self.objName, self.snapConfig, self.deadTree)
            snapReturn = self.snapInstance.create()
        except KeyError as err:
            self.deadTree.critical("No snapshot method found for '{0}'".format(self.method))
            exit(32)
        finally:
            if 'dbInstance' in self.snapConfig[self.method][self.objName]: self.freezer.unlock()

        return snapReturn

    def __exit__(self, *args):
        '''Context handler exiting method

        Destroys anything that was left open (in the case of LVM, the snapshot is removed).
        '''
        self.snapInstance.destroy()

        # The unlocking here should never be needed unless something explodes
        if 'dbInstance' in self.snapConfig[self.method][self.objName] and self.freezer.locked: self.freezer.unlock()

def plugin_wrapper(method, objName, snapConfig, treeInstance):
    '''The wrapper for snapshots, since it needs a context handler and some glue.

    This is a thread safe function designed for the Safestore plugin API, which calls it in 
    Core.backup().
    '''
    treeInstance.info('Mounting and backing up {0}'.format(objName))

    with MountFS(method, objName, snapConfig, treeInstance) as snap:
        coreConfig = {
            'backup' : {
                snap : {
                    'async' : {
                        'maxThreads' : 1,
                        'pollInterval': 0.01
                    },
                    'destination' : snapConfig[method][objName]['destination']
                }
            },
            'safestore' : {}
        }

        # Set anything custom that was passed
        if method.lower() == 'lvm':
            if 'async' in snapConfig[method][objName]:                
                if 'maxThreads' in snapConfig[method][objName]['async']:
                    coreConfig['backup'][snap]['async']['maxThreads'] = snapConfig[method][objName]['async']['maxThreads']
                if 'pollInterval' in snapConfig[method][objName]['async']:
                    coreConfig['backup'][snap]['async']['pollInterval'] = snapConfig[method][objName]['async']['pollInterval']
            if 'excludes' in snapConfig[method][objName]:
                [ 
                    coreConfig['backup'][snap]['excludes'].append(exclude) 
                    for exclude in snapConfig[method][objName]['excludes'] 
                ]
        
        if 'bwCap' in snapConfig[method][objName]:
            coreConfig['backup'][snap]['bwCap'] = snapConfig[method][objName]['bwCap']

        treeInstance.info('Running backup')
        treeInstance.debug('Core configuration is:\n{0}'.format(pformat(coreConfig)))
        # run a file backup of whatever path the snapshot is giving us
        snapCore = safestore.Core(coreConfig, treeInstance=treeInstance)
        snapCore.backup()