#!/usr/bin/env python3
# safestore.py
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see
# <http://www.gnu.org/licenses/>.
# Authors:
#     Trae Santiago <trae32566@gmail.com>
'''Backup program with rotation functionality

Uses rsync and rsyncd to backup and/or rotate a server's backups
'''

from argparse import ArgumentParser
from collections import OrderedDict
from datetime import datetime
from json import load
from logging import basicConfig, captureWarnings, getLogger, shutdown
from os.path import dirname, exists
from platform import machine
from pprint import pformat
import sys

# Python 3.4+
try:
    import importlib.util
# Python 2 - 3.3
except ImportError:
    import imp

# The uppoercase string below gets replaced by the packaging (ex: rpm specfile)
ssVersionInfo = "Safestore DEVEL using Python {0}.{1}.{2} ({3})".format(sys.version_info[0], sys.version_info[1], sys.version_info[2], machine())

def safestore_import(pluginName=None):
    # Figure out whether we live in a subdirectory, lib64, or lib
    if exists('{0}/safestore/__init__.py'.format(dirname(sys.argv[0]))): 
        ssPath = dirname(sys.argv[0])
    elif sys.maxsize > 2**32: 
        ssPath = '/usr/lib64/safestore'
    else: 
        ssPath = '/usr/lib/safestore'
    
    # Add any plugins
    if pluginName: 
        ssPath = '{0}/plugins/{1}'.format(ssPath, pluginName)
    # Load safestore if not
    else:
        pluginName = 'safestore'

    # Python 3.4+
    try:
        newModSpec = importlib.util.spec_from_file_location(pluginName, '{0}/{1}/__init__.py'.format(ssPath, pluginName))
        newMod = importlib.util.module_from_spec(newModSpec)
        sys.modules[pluginName] = newMod
        newModSpec.loader.exec_module(newMod)
        return newMod
    # Python 2 - 3.3
    except NameError:
        modInfo = imp.find_module(pluginName, [ssPath])
        return imp.load_module(pluginName, *modInfo)

def convert_cli(cliObject):
    '''Decorator function to convert CLI to config

    This function returns a function which, when called, returns a dictionary containing the
    equivalent to a JSON configuration file. This prevents the need to parse command line options in
    the Core class.
    '''
    config = {}
    def return_config():
        return OrderedDict(config.items())

    cliResult = cliObject()
    if isinstance(cliResult, dict):
        config = cliResult
        return return_config
    else:
        cliArgs = cliResult.__dict__

    # Grab, validate, and assure the command is not the verbosity flag
    try:
        action = sys.argv[3] if sys.argv[1][:6] == '--verb' else sys.argv[1]
    except IndexError:
        deadTree.info('Command not found; returning blank configuration!')
        return return_config

    if action == 'backup' or action == 'restore':
        # Mandatory stuff
        config[action] = {
            cliArgs['source'] : {
                'async' : {}, # F***ing python can't understand nested dictionary instantiation -_-
                'destination' : cliArgs['destination']
            }
        }

        # Optional flags
        if cliArgs['excludes']: config[action][cliArgs['source']]['excludes'] = cliArgs['excludes']
        if cliArgs['bwCap']: config[action][cliArgs['source']]['bwCap'] = cliArgs['bwCap']
        if cliArgs['passFile']: config[action][cliArgs['source']]['passFile'] = cliArgs['passFile']

        # Flags for restore only
        if 'date' in cliArgs: config[action][cliArgs['source']]['date'] = cliArgs['date']
        if 'list' in cliArgs: config[action][cliArgs['source']]['list'] = True
    elif action == 'rotate':
        config['rotate'][cliArgs['source']] = cliArgs['backupCount']

    config['safestore'] = {}

    # Multi-action optional flags
    if cliArgs['maxThreads']: config[action][cliArgs['source']]['async']['maxThreads'] = cliArgs['maxThreads']
    if cliArgs['pollInterval']: config[action][cliArgs['source']]['async']['pollInterval'] = cliArgs['pollInterval']
    if cliArgs['serial']: config['safestore']['serial'] = cliArgs['serial']
    if cliArgs['verbosity']: config['safestore']['verbosity'] = cliArgs['verbosity']

    return return_config

@convert_cli
def cli():
    '''Command line interface creator and parser

    Creates and parses the CLI, and is then wrapped by convert_cli() to produce a dictionary.
    See convert_cli().__docs__ for more info.
    '''
    # Get a dead tree setup ASAP -- cli() is actually invoked prior to __main__
    global deadTree, globalParser, safestore
    basicConfig(format='%(asctime)s | %(levelname)s - %(name)s.%(funcName)s @ %(lineno)d: %(message)s', level='DEBUG')
    captureWarnings(True)
    deadTree = getLogger('safestore')
    deadTree.setLevel('DEBUG')

    safestore = safestore_import()

    # Default configuration directory
    configDir='/etc/safestore'

    # Global argument parser
    globalParser = ArgumentParser(
        description='Backup program with backup, rotation, and restoration functionality',
        prog='safestore'
    )
    # str.upper makes it case insensitive, but always *returns* an uppercase result
    globalParser.add_argument(
        '--verbosity',
        choices=['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG'],
        help='Debug verbosity level',
        type=str.upper
    )
    globalParser.add_argument(
        '--serial',
        help='Run configurations in serial only mode (does not affect async settings)',
        type=bool
    )
    globalParser.add_argument('--config', help='Configuration file location')
    globalParser.add_argument('--version', action='version', version=ssVersionInfo)
    globalParser.add_argument('--maxThreads', help='Max number of threads to use (Python 2 only!)', type=int)
    globalParser.add_argument('--pollInterval', help='Polling interval to check threads', type=float)

    # Create subparser action instance
    subparsers = globalParser.add_subparsers(help='Action to take')
    #subparsers.required = True

    # Backup
    backupParser = subparsers.add_parser('backup', help='Runs an rsync of the directory given')
    backupParser.add_argument('source', help='Directory to backup')
    backupParser.add_argument('destination', help='Rsyncd destination host or directory')
    backupParser.add_argument('--bwCap', help='Bandwidth cap (in KBPS) to limit the operation to', type=int)
    backupParser.add_argument('--passFile', help='File containing the destination host password')
    backupParser.add_argument('--excludes', help='File and directory exclusion list', nargs='+')

    # Rotate
    rotateParser = subparsers.add_parser('rotate', help='Deletes the oldest backups')
    rotateParser.add_argument('source', help='Backup directory to rotate')
    rotateParser.add_argument('backupCount', help='Backup directory to rotate')

    # Restore
    restoreParser = subparsers.add_parser('restore', help='Restores a Safestore backup')
    restoreParser.add_argument('source', help='Directory to restore from')
    restoreParser.add_argument('destination', help='Destination host or directory')
    restoreParser.add_argument('--bwCap', help='Bandwidth cap (in KBPS) to limit the operation to', type=int)
    restoreParser.add_argument(
        '--date',
        help='Backup date to restore from',
        type=lambda bkpDate: datetime.strptime(bkpDate, '%Y-%m-%d_%H-%M')
    )
    restoreParser.add_argument('--list', action='store_true', help='List available backups')
    restoreParser.add_argument('--passFile', help='File containing the destination host password')
    restoreParser.add_argument('--excludes', help='File and directory exclusion list', nargs='+')

    # Decides whether to use a config file or arguments
    if len(sys.argv) == 2 and sys.argv[1] in ['backup', 'rotate'] or '--config' in sys.argv:
        deadTree.info('Loading from configuration file, all other arguments ignored!')
        try:
            if '--config' in sys.argv: configFile = sys.argv[sys.argv.index('--config') + 1]
            else: configFile = '{0}/safestore.json'.format(configDir)

            with open(configFile) as config:
                return load(config)
        except IOError as err:
            raise safestore.exceptions.ConfigError('{0}/safestore.json'.format(configDir), err, sys.exc_info(), deadTree)
        except ValueError as err:
            raise safestore.exceptions.ConfigError('{0}/safestore.json'.format(configDir), err, sys.exc_info(), deadTree)
    else:
        resultConfig = globalParser.parse_args()
        return resultConfig

def main():
    deadTree.info(ssVersionInfo)
    deadTree.info('Parsing command line options')
    coreConfig = cli()

    # Set debugging level
    if 'safestore' in coreConfig and 'verbosity' in coreConfig['safestore']:
       deadTree.setLevel(coreConfig['safestore']['verbosity'])

    deadTree.debug("Core configuration: \n{0}".format(pformat(coreConfig, width=1)))
    deadTree.info('Instantiating core handler')
    coreInstance = safestore.Core(coreConfig, treeInstance=deadTree)

    # Choose the action based on the first argument
    coreActions = {
        'backup'    : coreInstance.backup,
        'restore'   : coreInstance.restore,
        'rotate'    : coreInstance.rotate
    }

    try:
        action = sys.argv[3] if sys.argv[1][:6] == '--verb' else sys.argv[1]

        if action in coreActions: coreActions[action]()
        else: deadTree.error("Invalid command: '{0}'!".format(sys.argv[1]))
    except IndexError:
        deadTree.critical('Command missing!')
        globalParser.print_help()
        shutdown()
        exit(1)

    shutdown()

if __name__ == '__main__':
    main()